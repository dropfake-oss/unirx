﻿using System;

#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine.Profiling;
#endif

namespace UniRx.InternalUtil
{
    public class ThreadSafeQueueWorker
    {
        const int MaxArrayLength = 0X7FEFFFFF;
        const int InitialSize = 16;

        object gate = new object();
        bool dequing = false;

        int actionListCount = 0;
        Action<object>[] actionList = new Action<object>[InitialSize];
        object[] actionStates = new object[InitialSize];

        int waitingListCount = 0;
        Action<object>[] waitingList = new Action<object>[InitialSize];
        object[] waitingStates = new object[InitialSize];
        
#if UNITY_EDITOR
        Dictionary<(Action<object>, object), string> profileSampleNames = new Dictionary<(Action<object>, object), string>();
#endif

        public void Enqueue(Action<object> action, object state)
        {
            lock (gate)
            {
                if (dequing)
                {
                    // Ensure Capacity
                    if (waitingList.Length == waitingListCount)
                    {
                        var newLength = waitingListCount * 2;
                        if ((uint)newLength > MaxArrayLength) newLength = MaxArrayLength;

                        var newArray = new Action<object>[newLength];
                        var newArrayState = new object[newLength];
                        Array.Copy(waitingList, newArray, waitingListCount);
                        Array.Copy(waitingStates, newArrayState, waitingListCount);
                        waitingList = newArray;
                        waitingStates = newArrayState;
                    }
                    waitingList[waitingListCount] = action;
                    waitingStates[waitingListCount] = state;
                    waitingListCount++;
                }
                else
                {
                    // Ensure Capacity
                    if (actionList.Length == actionListCount)
                    {
                        var newLength = actionListCount * 2;
                        if ((uint)newLength > MaxArrayLength) newLength = MaxArrayLength;

                        var newArray = new Action<object>[newLength];
                        var newArrayState = new object[newLength];
                        Array.Copy(actionList, newArray, actionListCount);
                        Array.Copy(actionStates, newArrayState, actionListCount);
                        actionList = newArray;
                        actionStates = newArrayState;
                    }
                    actionList[actionListCount] = action;
                    actionStates[actionListCount] = state;
                    actionListCount++;
                }
            }
        }

        public void ExecuteAll(Action<Exception> unhandledExceptionCallback)
        {
            lock (gate)
            {
                if (actionListCount == 0) return;

                dequing = true;
            }

#if UNITY_EDITOR
            Profiler.BeginSample("Actions");
#endif
            for (int i = 0; i < actionListCount; i++)
            {
                var action = actionList[i];
                var state = actionStates[i];
                try
                {
#if UNITY_EDITOR
                    var sampleKey = (action, state);
                    if (!profileSampleNames.TryGetValue(sampleKey, out var sampleName))
                    {
                        sampleName = "State: " + state;
                        profileSampleNames.Add(sampleKey, sampleName);
                    }
                    Profiler.BeginSample(sampleName);
#endif
                    action(state);
                }
                catch (Exception ex)
                {
                    unhandledExceptionCallback(ex);
                }
                finally
                {
                    // Clear
                    actionList[i] = null;
                    actionStates[i] = null;
#if UNITY_EDITOR
                    Profiler.EndSample();
#endif  
                }
            }
#if UNITY_EDITOR
            Profiler.EndSample();
#endif

            lock (gate)
            {
                dequing = false;

                var swapTempActionList = actionList;
                var swapTempActionStates = actionStates;

                actionListCount = waitingListCount;
                actionList = waitingList;
                actionStates = waitingStates;

                waitingListCount = 0;
                waitingList = swapTempActionList;
                waitingStates = swapTempActionStates;
            }
        }
    }
}