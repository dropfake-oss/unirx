﻿using System;

namespace UniRx
{
	public class MainThreadBehaviorSubject<T> : BehaviorSubject<T>
	{
		public MainThreadBehaviorSubject(T defaultValue)
			:
		base(defaultValue)
		{
		}

		public IObservable<T> AsObservable()
		{
#if UNITY_2017_2_OR_NEWER
			return this.ObserveOnMainThread();
#else
			return this;
#endif
		}
	}
}
