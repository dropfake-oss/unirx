﻿using System;
using System.Collections.Generic;

namespace UniRx
{
	public class IfChangedBehaviorSubject<T> : BehaviorSubject<T>
	{
		public IfChangedBehaviorSubject(T defaultValue)
			:
		base(defaultValue)
		{
		}

		public new void OnNext(T value)
		{
			var same = EqualityComparer<T>.Default.Equals(value, this.Value);
			if (same == true)
			{
				return;
			}

			base.OnNext(value);
		}
	}
}
