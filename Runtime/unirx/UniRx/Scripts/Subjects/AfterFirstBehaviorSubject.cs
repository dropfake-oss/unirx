﻿namespace UniRx
{
	public class AfterFirstBehaviorSubject<T> : BehaviorSubject<T>
	{
		public bool UpdatedSinceCreation { get; private set; } = false;
		public override bool CanSendOnSubscription => UpdatedSinceCreation;

		public AfterFirstBehaviorSubject(T defaultValue)
			:
		base(defaultValue)
		{
		}

		public new void OnNext(T value)
		{
			UpdatedSinceCreation = true;
			base.OnNext(value);
		}
	}
}
